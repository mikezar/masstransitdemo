﻿using System;

namespace MassTransitDemo.Contracts.Events
{
    public interface ISmsSent
    {
        Guid EventId { get; }
        DateTime SendDateTime { get; }
    }
}
