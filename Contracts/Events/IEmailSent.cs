﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MassTransitDemo.Contracts.Events
{
    public interface IEmailSent
    {
        Guid EventId { get; }
        DateTime SendDateTime { get; }
        bool IsReceived { get; }
    }
}
