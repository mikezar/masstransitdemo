﻿using System;

namespace MassTransitDemo.Contracts.Commands
{
    public class SmsSend : ISmsSend
    {
        public Guid CommandId { get; set; }
        public string Phone { get; set; }
        public string Message { get; set; }
    }
}
