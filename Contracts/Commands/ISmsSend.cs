﻿using System;

namespace MassTransitDemo.Contracts.Commands
{
    public interface ISmsSend
    {
        Guid CommandId { get; }
        string Phone { get; }

        string Message { get; }
    }
}
