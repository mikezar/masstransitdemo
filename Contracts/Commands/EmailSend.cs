﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MassTransitDemo.Contracts.Commands
{
    public class EmailSend : IEmailSend
    {
        public Guid CommandId { get; set; }

        public string Subject { get; set; }

        public string Email { get; set; }

        public string Body { get; set; }
    }
}
