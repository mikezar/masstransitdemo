﻿using System;

namespace MassTransitDemo.Contracts.Commands
{
    public interface IEmailSend
    {
        Guid CommandId { get; }
        string Subject { get; }

        string Email { get; }

        string Body { get; }
    }
}
