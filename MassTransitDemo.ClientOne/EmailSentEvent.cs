﻿using System;
using System.Threading.Tasks;
using MassTransit;
using MassTransitDemo.Contracts.Events;

namespace MassTransitDemo.ClientOne
{
   public class EmailSentEvent : IConsumer<IEmailSent>
    {
        public async Task Consume(ConsumeContext<IEmailSent> context)
        {
            var message = context.Message; 
            Console.WriteLine($"[ConsumerTwo] Email event send");
            Console.WriteLine($"[ConsumerTwo] EventId: {message.EventId}");
            Console.WriteLine($"[ConsumerTwo] SendDateTime: {message.SendDateTime}");
            Console.WriteLine($"[ConsumerTwo] IsReceived: {(message.IsReceived == true ? "Yes" : "No")}");



            await Task.FromResult(0);
        }
    }
}
