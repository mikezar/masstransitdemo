﻿using System;
using System.Threading.Tasks;
using MassTransit;

namespace MassTransitDemo.ClientOne
{
        public class ConsumeObserver : IConsumeObserver
        {
            public Task PreConsume<T>(ConsumeContext<T> context) where T : class
            {
                Console.WriteLine($"[IConsumeObserver] PreConsume {context.MessageId}");
                return Task.FromResult(0);
            }

            public Task PostConsume<T>(ConsumeContext<T> context) where T : class
            {
                Console.WriteLine($"[IConsumeObserver] PostConsume {context.MessageId}");
                return Task.FromResult(0);
                

            }

            public Task ConsumeFault<T>(ConsumeContext<T> context, Exception exception) where T : class
            {
                Console.WriteLine($"[IConsumeObserver] ConsumeFault {context.MessageId}");
                return Task.FromResult(0);
            }
        }
}
