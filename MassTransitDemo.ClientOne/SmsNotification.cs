﻿using System;
using System.Threading.Tasks;
using MassTransit;
using MassTransitDemo.Contracts.Commands;
using MassTransitDemo.Contracts.Events;

namespace MassTransitDemo.ClientOne
{
    public class SmsNotification : IConsumer<ISmsSend>
    {
        public async Task Consume(ConsumeContext<ISmsSend> context)
        {
            var message = context.Message;
            Console.WriteLine($"[Consumer] SMS send");
            Console.WriteLine($"[Consumer] CommandId: {message.CommandId}");
            Console.WriteLine($"[Consumer] Phone: {message.Phone}");
            Console.WriteLine($"[Consumer] Message: {message.Message}");

            await context.RespondAsync<ISmsSent>(new
            {
                EventId = Guid.NewGuid(),
                SendDateTime = DateTime.Now
            });
        }
    }
}
