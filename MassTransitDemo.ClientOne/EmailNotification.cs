﻿using System;
using System.Threading.Tasks;
using MassTransit;
using MassTransitDemo.Contracts.Commands;
using MassTransitDemo.Contracts.Events;


namespace MassTransitDemo.ClientOne
{
    public class EmailNotification : IConsumer<IEmailSend>
    {
        public async Task Consume(ConsumeContext<IEmailSend> context)
        {
            var message = context.Message;
            Console.WriteLine($"[ConsumerTwo] Email send");
            Console.WriteLine($"[ConsumerTwo] CommandId: {message.CommandId}");
            Console.WriteLine($"[ConsumerTwo] Subject: {message.Subject}");
            Console.WriteLine($"[ConsumerTwo] Email: {message.Email}");
            Console.WriteLine($"[ConsumerTwo] Body: {message.Body}");

            await Task.FromResult(0);
        }
    }
}
