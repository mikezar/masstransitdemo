﻿using System;
using System.Threading.Tasks;
using MassTransit;
using MassTransitDemo.Contracts.Events;

namespace MassTransitDemo.ClientOne
{
    public class SmsSentEvent : IConsumer<ISmsSent>
    {
        public async Task Consume(ConsumeContext<ISmsSent> context)
        {
            var message = context.Message;
            Console.WriteLine($"[Consumer] SMS event send");
            Console.WriteLine($"[Consumer] EventId: {message.EventId}");
            Console.WriteLine($"[Consumer] SendDateTime: {message.SendDateTime}");

            await Task.FromResult(0);
        }
    }
}
