﻿using System;
using System.Threading.Tasks;
using MassTransit;
using MassTransitDemo.Contracts.Commands;
using MassTransitDemo.Contracts.Events;
using MassTransitDemo.MessageBus;

namespace MassTransitDemo.CLI
{
    public class Program
    {

        public static void Main(string[] args)
        {
            var controlBus = new BusConfiguration();

            controlBus.ControlBus.Start();

            Console.WriteLine("Async method call");
            Task.Run(async () => await controlBus.SendMessage(new EmailSend()
            {
                CommandId = Guid.NewGuid(),
                Subject = "Help",
                Email = "test@test.com",
                Body = $"Test email message #{new Random().Next(0, 1000)}"
            })).Wait();

            Console.ReadKey();

            Console.WriteLine("RPC method call");
            var task = controlBus.RPCCall<ISmsSend, ISmsSent>(new SmsSend
            {
                CommandId = Guid.NewGuid(),
                Phone = "8-906-394-31-34",
                Message = $"Test sms message #{new Random().Next(0, 1000)}"
            });
            task.Wait();

            Console.WriteLine($"[RESPONCE] {task.Result.EventId}");

            Console.ReadKey();

            Console.WriteLine("Publish/Subscribe");
            Task.Run(async () => await controlBus.Publish<ISmsSend>(new EmailSend()
            {
                CommandId = Guid.NewGuid(),
                Subject = "Help",
                Email = "test@test.com",
                Body = $"Test email message #{new Random().Next(0, 1000)}"
            })).Wait();

            Console.ReadKey();

            controlBus.ControlBus.Stop();
        }
    }
}
