﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MassTransit;
using MassTransitDemo.Contracts.Commands;
using MassTransitDemo.Contracts.Events;

namespace MassTransitDemo.ClientTwo
{
    public class SendNotificationTwo : IConsumer<ISmsSend>
    {
        public async Task Consume(ConsumeContext<ISmsSend> context)
        {
            var message = context.Message;
            Console.WriteLine($"[ConsumerTwo] SMS send");
            Console.WriteLine($"[ConsumerTwo] CommandId: {message.CommandId}");
            Console.WriteLine($"[ConsumerTwo] Phone: {message.Phone}");
            Console.WriteLine($"[ConsumerTwo] Message: {message.Message}");

            await context.RespondAsync<ISmsSent>(new
            {
                EventId = Guid.NewGuid(),
                SendDateTime = DateTime.Now
            });

            //await context.Publish<ISmsSent>(new
            //{
            //    EventId = Guid.NewGuid(),
            //    SendDateTime = DateTime.Now
            //});
            //await Task.FromResult(0);
        }
    }
}
