﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MassTransit;
using MassTransitDemo.ClientOne;
using MassTransitDemo.Contracts.Events;
using MassTransit.RabbitMqTransport;

namespace MassTransitDemo.MessageBus
{
    public class BusConfiguration
    {
        private readonly IBusControl _bus;
        private IRabbitMqHost _host;

        public IBusControl ControlBus => _bus;

        public BusConfiguration()
        {
            _bus = ConfigureBus();
            _bus.ConnectConsumeObserver(new ConsumeObserver());
            // var handle = _bus.ConnectConsumer<EmailNotification>();
            //_host.ConnectReceiveEndpoint("sms_queue_event", e =>
            //{
            //    e.Consumer<SmsSentEvent>();
            //    e.Consumer<EmailSentEvent>();
            //});
        }

        private IBusControl ConfigureBus()
        {
            return Bus.Factory.CreateUsingRabbitMq(cfg =>
            {
                _host = cfg.Host(new Uri("rabbitmq://localhost/"), h =>
                {
                    h.Username("guest");
                    h.Password("guest");
                });

                cfg.ReceiveEndpoint(_host, "sms_queue", e =>
                {
                    e.Consumer<SmsNotification>();
                    e.Consumer<EmailNotification>();
                });
                cfg.ReceiveEndpoint(_host, "sms_queue_event", e =>
                {
                    e.Consumer<SmsSentEvent>();
                    e.Consumer<EmailSentEvent>();
                });
            });

        }

        public async Task<TResponse> RPCCall<TRequest, TResponse>(TRequest command) where TResponse : class where TRequest : class
        {
            var client = new MessageRequestClient<TRequest, TResponse>(
                _bus, 
                new Uri("rabbitmq://localhost/sms_queue"),
                TimeSpan.FromMinutes(1));
            return await client.Request(command, CancellationToken.None);
        }

        public async Task SendMessage<T>(T command) where T : class
        {
            var endpoint = await _bus.GetSendEndpoint(new Uri("rabbitmq://localhost/sms_queue"));
            await endpoint.Send(command);
        }

        public async Task PublishEvent<T>() where T : class
        {
            await _bus.Publish<T>(new
            {
                EventId = Guid.NewGuid(),
                SendDateTime = DateTime.Now
            });
        }

        public async Task Publish<T>(object command) where T : class
        {
            await _bus.Publish<T>(command);
        }
    }
}
